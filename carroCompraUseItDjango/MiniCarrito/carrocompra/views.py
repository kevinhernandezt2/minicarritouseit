from django.shortcuts import render, redirect
from django.core.paginator import Paginator
from django.contrib.auth.forms import UserCreationForm 
from carrocompra.forms import RegisterForm
from django.contrib import messages 
from django.contrib.auth import authenticate, login, logout 
from carrocompra.models import  CarrocompraProducto, CarrocompraAgregarCarrito, CarrocompraComprarCarrito, AuthUser
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.db.models import Avg, Max, Min, Sum

# Create your views here.

# metodo de Inicio
def inicio(request):

    return render(request, 'menuapp/inicio.html', {
        'titulo': 'Inicio'
    })

#metodo registrar pagina usuario
def registrar_pagina(request):
    
    if request.user.is_authenticated:
        return redirect('inicio')
    else:

        register_form = RegisterForm() 
        
        if request.method == 'POST':
            register_form = RegisterForm(request.POST)

            if register_form.is_valid():
                register_form.save()
                messages.success(request, 'Tu Registro ha sido Exitoso') 

                return redirect('inicio')

        return render(request, 'usuarios/registro.html', {
            'titulo': 'Registro Usuario',
            'registrar_formulario': register_form
        })

#metodo login usuario
def login_pagina(request):

    if request.user.is_authenticated:
        return redirect('inicio')
    else:
        
        if request.method == 'POST':
            
            
            nombre_usuario = request.POST.get("username")
            contrasena = request.POST.get("password")

            usuario = authenticate(request, username=nombre_usuario, password=contrasena)

            if usuario != None:
                login(request, usuario)
                messages.success(request, f"Bienvenido {nombre_usuario}")
                return redirect('inicio')
            
            else:
                messages.warning(request, 'No te has identificado correctamente :(')

        return render(request, 'usuarios/login.html', {
            'tituloLogin': 'Identificate'
        })

#metodo cierre de sesion
def logout_usuario(request):
    logout(request) 
    return redirect('/login')

#metodo que lista todos los productos
@login_required(login_url="logueo")
def listado_productos(request):
    
    consulta_productos = CarrocompraProducto.objects.all()

   
    paginador = Paginator(consulta_productos, 4)

    
    numero_pagina = request.GET.get('page')
    numero_pagina_productos = paginador.get_page(numero_pagina)
    
    return render(request, 'articulos/Productos.html',{
        'titulo': 'Productos',
        'listaProductos': numero_pagina_productos 
    })

#metodo que agrega o almacena al carro de compras
@login_required(login_url="logueo")
def agregar_carrito(request):

    if request.method == 'POST':

        cu = request.POST['codusuario']
        cp = request.POST['codigoProducto']
        pproducto = int(request.POST['precioProducto'])
        cant = int(request.POST['cantidad'])
        
        
        pk_usuario = AuthUser(
        id = cu
        )
        
        pk_producto = CarrocompraProducto(
        id_producto = cp
        )

        isPrecioTotalCantidad = pproducto * cant
        
        agregar_carro_producto = CarrocompraAgregarCarrito(
        fk_user = pk_usuario,
        fk_producto = pk_producto,
        cantidad_agrega = cant,
        precio_total_agrega = isPrecioTotalCantidad
        
        )

        if (agregar_carro_producto.fk_user == "" or agregar_carro_producto.fk_user == None or agregar_carro_producto.fk_user == 0) and (agregar_carro_producto.fk_producto == "" or agregar_carro_producto.fk_producto == None or agregar_carro_producto.fk_producto == 0) and (agregar_carro_producto.cantidad_agrega == "" or agregar_carro_producto.cantidad_agrega == None or agregar_carro_producto.cantidad_agrega == 0):
            messages.error(request, "Error seleccionar algun campo o campos vacios")
            return redirect('todosLosProductos')
        elif agregar_carro_producto.fk_user == "" or agregar_carro_producto.fk_user == None or agregar_carro_producto.fk_user == 0:
            messages.error(request, "Error campo vacio usuario")
            return redirect('todosLosProductos')
        elif agregar_carro_producto.fk_producto == "" or agregar_carro_producto.fk_producto == None or agregar_carro_producto.fk_producto == 0:
            messages.error(request, "Error campo vacio producto")
            return redirect('todosLosProductos')
        elif agregar_carro_producto.cantidad_agrega == "" or agregar_carro_producto.cantidad_agrega == None or agregar_carro_producto.cantidad_agrega == 0:
            messages.error(request, "Error campo vacio cantidad")
            return redirect('todosLosProductos')
        

        agregar_carro_producto.save()
        messages.success(request, "PRODUCTO AGREGADO AL CARRITO DE COMPRA")

        return redirect('todosLosProductos')
    else:
            
        messages.error(request, "HUBO ALGUN ERROR AL AGREGAR EL CARRO DE COMPRA")
        return redirect('todosLosProductos')

#metodo que valida consulta en tabla CarrocompraProducto, pasando como parametro para que asi mismo se pueda hacer el retorno, en este caso valida para agregar al carro
@login_required(login_url="logueo")
def valida_id_producto(request, codigoProducto):

    consulta_id_producto = CarrocompraProducto.objects.get(id_producto=codigoProducto)

    return render(request, 'articulos/FormularioAgregaAlCarro.html', {
        
        'idProducto': consulta_id_producto
    })

#metodo que compra el producto
@login_required(login_url="logueo")
def comprar_producto(request):

    if request.method == 'POST':

        cu = request.POST['codusuario']
        cp = request.POST['codigoProducto']
        pproducto = int(request.POST['precioProducto'])
        cant = int(request.POST['cantidad'])
        
        
        pk_usuario = AuthUser(
        id = cu
        )
        
        pk_producto = CarrocompraProducto(
        id_producto = cp
        )

        isPrecioTotalCantidad = pproducto * cant
        
        comprar_producto = CarrocompraComprarCarrito(
        fk_user = pk_usuario,
        fk_producto = pk_producto,
        cantidad_compra = cant,
        precio_total_compra = isPrecioTotalCantidad
        
        )

        if (comprar_producto.fk_user == "" or comprar_producto.fk_user == None or comprar_producto.fk_user == 0) and (comprar_producto.fk_producto == "" or comprar_producto.fk_producto == None or comprar_producto.fk_producto == 0) and (comprar_producto.cantidad_compra == "" or comprar_producto.cantidad_compra == None or comprar_producto.cantidad_compra == 0):
            messages.error(request, "Error seleccionar algun campo o campos vacios")
            return redirect('todosLosProductos')
        elif comprar_producto.fk_user == "" or comprar_producto.fk_user == None or comprar_producto.fk_user == 0:
            messages.error(request, "Error campo vacio usuario")
            return redirect('todosLosProductos')
        elif comprar_producto.fk_producto == "" or comprar_producto.fk_producto == None or comprar_producto.fk_producto == 0:
            messages.error(request, "Error campo vacio producto")
            return redirect('todosLosProductos')
        elif comprar_producto.cantidad_compra == "" or comprar_producto.cantidad_compra == None or comprar_producto.cantidad_compra == 0:
            messages.error(request, "Error campo vacio cantidad")
            return redirect('todosLosProductos')
        

        comprar_producto.save()
        messages.success(request, "CONFIRMACION DE COMPRA EXITOSA, MUCHAS GRACIAS POR SU COMPRA")

        return redirect('todosLosProductos')
    else:
            
        messages.error(request, "HUBO ALGUN ERROR EN LA COMPRA")
        return redirect('todosLosProductos')

#metodo que valida consulta en tabla CarrocompraProducto, pasando como parametro para que asi mismo se pueda hacer el retorno, en este caso valida para la compra
@login_required(login_url="logueo")
def valida_id_producto_compra(request, codigoProducto):

    consulta_id_producto = CarrocompraProducto.objects.get(id_producto=codigoProducto)

    return render(request, 'articulos/FormularioAgregaAlCarro.html', {
        
        'idProductoCompra': consulta_id_producto
    })    

#metodo que lista por codigo de usuario, los productos que el usuario ha agregado al carrito, junto con la suma y cantidad total
@login_required(login_url="logueo")
def listado_carrito_compras(request, codigoUsuario):

    consulta_carrito_compras = CarrocompraAgregarCarrito.objects.all().filter(fk_user=codigoUsuario)

    suma_total_valida_carro = CarrocompraAgregarCarrito.objects.filter(fk_user = codigoUsuario).aggregate(Sum('precio_total_agrega'))

    cantidad_productos_agregados = CarrocompraAgregarCarrito.objects.filter(fk_user = codigoUsuario).aggregate(Sum('cantidad_agrega'))
    
    return render(request, 'articulos/CarroCompras.html',{
        'titulo': 'Carro de compras - Productos añadidos',
        'listaCarroCompras': consulta_carrito_compras,
        'sumaTotalCarroCompras__sum': suma_total_valida_carro,
        'cantidadProductos__sum': cantidad_productos_agregados
    })

#metodo que valida consulta en tabla CarrocompraAgregarCarrito, pasando como parametro para que asi mismo se pueda hacer el retorno, en este caso valida para
#compra estando en el carro de compras
@login_required(login_url="logueo")
def isValidaProductoEnCarritoParaCompra(request, codigoAgregarCarrito):

    consulta_id_producto_carrito = CarrocompraAgregarCarrito.objects.get(id_agregar_carrito=codigoAgregarCarrito)

    return render(request, 'articulos/FormularioAgregaAlCarro.html', {
        
        'idProductoEnCarrito': consulta_id_producto_carrito
    })  

#metodo que guarda compra de producto estando en carro de compra
@login_required(login_url="logueo")
def comprarProductoAgregadoAlCarro(request):

    if request.method == 'POST':

        cu = request.POST['codusuario']
        cp = request.POST['codigoProducto']
        cant = int(request.POST['cantidad'])
        totalidadCompra = int(request.POST['totalCompraProducto'])

        pk_usuario = AuthUser(
        id = cu
        )
        
        pk_producto = CarrocompraProducto(
        id_producto = cp
        )
        
        comprar_producto = CarrocompraComprarCarrito(
        fk_user = pk_usuario,
        fk_producto = pk_producto,
        cantidad_compra = cant,
        precio_total_compra = totalidadCompra
        
        )

        if (comprar_producto.fk_user == "" or comprar_producto.fk_user == None or comprar_producto.fk_user == 0) and (comprar_producto.fk_producto == "" or comprar_producto.fk_producto == None or comprar_producto.fk_producto == 0) and (comprar_producto.cantidad_compra == "" or comprar_producto.cantidad_compra == None or comprar_producto.cantidad_compra == 0) and (comprar_producto.precio_total_compra == "" or comprar_producto.precio_total_compra == None or comprar_producto.precio_total_compra == 0):
            messages.error(request, "Error seleccionar algun campo o campos vacios")
            return redirect('todosLosProductos')
        elif comprar_producto.fk_user == "" or comprar_producto.fk_user == None or comprar_producto.fk_user == 0:
            messages.error(request, "Error campo vacio usuario")
            return redirect('todosLosProductos')
        elif comprar_producto.fk_producto == "" or comprar_producto.fk_producto == None or comprar_producto.fk_producto == 0:
            messages.error(request, "Error campo vacio producto")
            return redirect('todosLosProductos')
        elif comprar_producto.cantidad_compra == "" or comprar_producto.cantidad_compra == None or comprar_producto.cantidad_compra == 0:
            messages.error(request, "Error campo vacio cantidad")
            return redirect('todosLosProductos')
        elif comprar_producto.precio_total_compra == "" or comprar_producto.precio_total_compra == None or comprar_producto.precio_total_compra == 0:
            messages.error(request, "Error campo vacio precio total")
            return redirect('todosLosProductos')

        comprar_producto.save()
        messages.success(request, f"SU COMPRA HA SIDO EXITOSA, POR FAVOR ELIMINE O QUITE LA COMPRA DEL CARRITO, CODIGO_PRODUCTO: {cp} , CANTIDAD: {cant} , TOTALIDAD: {totalidadCompra}")

        return redirect('todosLosProductos')
    else:
            
        messages.error(request, "HUBO ALGUN ERROR EN LA COMPRA DEL PRODUCTO AGREGADO AL CARRO DE COMPRAS")
        return redirect('todosLosProductos')

#metodo que elimina o quita el producto que esta en el carro de compra
@login_required(login_url="logueo")
def quitarProductoCarroCompras(request, codigoId):
    
    consultaPorIdEnAgregarCarrito = CarrocompraAgregarCarrito.objects.get(id_agregar_carrito=codigoId)

    consultaPorIdEnAgregarCarrito.delete()
    messages.warning(request, "SE HA QUITADO EL PRODUCTO DEL CARRITO DE COMPRA")

    return redirect('todosLosProductos')