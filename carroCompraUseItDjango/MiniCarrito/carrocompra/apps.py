from django.apps import AppConfig


class CarrocompraConfig(AppConfig):
    name = 'carrocompra'
    verbose_name = 'Gestion Carro de Compra'