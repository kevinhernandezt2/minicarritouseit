from django.urls import path
from . import views 

#se crea archivo de urls en esta app, para que no se almacenaran en archivo settings del elemento principal, urls llamados del views al template
urlpatterns = [
    path('', views.inicio, name="index"),
    path('inicio/', views.inicio, name="inicio"),
    path('registro/', views.registrar_pagina, name="registro"),
    path('login/', views.login_pagina, name="logueo"),
    path('cierreSesion/', views.logout_usuario, name="logout"),
    path('productos/', views.listado_productos, name="todosLosProductos"),
    path('formAgregarAlCarro/', views.agregar_carrito, name="agregaralcarro"),
    path('formValidaIdProducto/<int:codigoProducto>', views.valida_id_producto, name="validaagregaralcarro"),
    path('formCompraProducto/', views.comprar_producto, name="compraproducto"),
    path('formValidaIdProductoCompra/<int:codigoProducto>', views.valida_id_producto_compra, name="validacompraproducto"),
    path('carroCompras/<int:codigoUsuario>', views.listado_carrito_compras, name="todosproductosalcarro"),
    path('formCompraProductoEnCarroCompra/', views.comprarProductoAgregadoAlCarro, name="compraproductoencarro"),
    path('formValidaProductoParaCompraEnCarro/<int:codigoAgregarCarrito>', views.isValidaProductoEnCarritoParaCompra, name="validacompraproductoencarro"),
    path('eliminarProducto/<int:codigoId>', views.quitarProductoCarroCompras, name="eliminarproducto")

]